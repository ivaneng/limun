import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(niz: any, searchQuery: any): any {
    return searchQuery
    ? niz.filter(jedanElementNiza => {
        return (
          String(jedanElementNiza.nutritionValue)
            .toLowerCase()
            .indexOf(searchQuery.toLowerCase()) > -1 ||
          String(jedanElementNiza.weight)
            .toLowerCase()
            .indexOf(searchQuery.toLowerCase()) > -1 ||
          String(jedanElementNiza.color)
            .toLowerCase()
            .indexOf(searchQuery.toLowerCase()) > -1
        );
      })
    : niz;
}
}

