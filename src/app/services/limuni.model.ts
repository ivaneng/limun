import { Injectable } from "@angular/core";
import { Limun } from "../interfaces/limun";
import { LimuniService } from "./limuni.services";
@Injectable()
export class LimuniModel {
  limuni = [];
  constructor(private service: LimuniService) {
    this.refresujLimune();
  }

  refresujLimune() {
    this.service.getLimunObservable().subscribe(limuni => {
      this.limuni = limuni;
    });
  }

  refresujLimuneClbk(clbk) {
    this.service.getLimunObservable().subscribe(limuni => {
      this.limuni = limuni;
      clbk();
    });
  }
  obrisiLimun(id, callbackFunkcija) {
    this.service.deleteJedanLimunObservable(id).subscribe(() => {
      this.refresujLimune();
      callbackFunkcija();
    });
  }
  dodajLimun(limun, clbk) {
    this.service.addjedanLimunObservable(limun).subscribe(limun => {
      this.limuni.push(limun);
      clbk();
    });
  }
  dajLimunPoId(id, clbk) {
    if (this.limuni && this.limuni.length > 0) {
      for (var i = 0; i < this.limuni.length; i++)
        if (this.limuni[i].id == id) {
          clbk(this.limuni[i]);
        } 
        
    }else {
      this.refresujLimuneClbk(()=>{
        for (var i = 0; i < this.limuni.length; i++)
        if (this.limuni[i].id == id) {
          clbk(this.limuni[i]);
        } 
      });
    } }
  
}
