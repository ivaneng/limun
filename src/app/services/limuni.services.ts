import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class LimuniService {
  constructor(private http: Http) {}

  getLimunObservable() {
    return this.http.get("http://localhost:3000/limuni").map(response => {
      return response.json();
    });
  }
  getjedanLimunObservable(id) {
    return this.http.get("http://localhost:3000/limuni/" + id).map(response => {
      return response.json();
    });
  }
  addjedanLimunObservable(limun) {
    return this.http
      .post("http://localhost:3000/limuni", limun)
      .map(response => {
        return response.json();
      });
  }
  updateLimunObservable(limun) {
    return this.http
      .put("http://localhost:3000/limuni/" + limun.id, limun)
      .map(response => {
        return response.json();
      });
  }
  deleteJedanLimunObservable(id) {
    return this.http
      .delete("http://localhost:3000/limuni/" + id)
      .map(response => {
        return response.json();
      });
  }
}
