export interface Limun {

    id:number,
    nutritionValue:number,
    weight:string,
    color?:string
}