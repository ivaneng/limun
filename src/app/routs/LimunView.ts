import { Component } from '@angular/core';
import { LimuniModel } from '../services/limuni.model';
import { Router } from '@angular/router';
@Component({
    templateUrl: './LimunView.html'
})


export class LimunView {
    constructor(public model: LimuniModel, private router: Router) { }
    otvoriLimun(id) {
        this.router.navigate(['/limuni', id]);
    }
    editujLimun(id) {
        this.router.navigate(['/limuni', id, 'edit']);
    }
    obrisiLimun(id) {
        this.router.navigate(['/limuni', id, 'delete']);
    }
}