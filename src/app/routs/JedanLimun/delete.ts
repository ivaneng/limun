import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LimuniModel } from "../../services/limuni.model";
@Component({
  templateUrl: "./delete.html"
})
export class JedanLimunDelete implements OnInit {
  id;

  constructor(
    private limuniModel: LimuniModel,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.parent.params.subscribe(params => (this.id = params.id));
  }

  obrisiLimun() {
    this.limuniModel.obrisiLimun(this.id, ()=>{this.router.navigate(['/limuni'])});

  }
}
