import { Component } from "@angular/core";
import { colors } from "../../configs/colors";
import { ActivatedRoute, Router } from "@angular/router";
import { LimuniModel } from "../../services/limuni.model";
@Component({
  templateUrl: "./edit.html"
})
export class Edit {
  id;
  limun={};
  constructor(
    private limuniModel: LimuniModel,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.parent.params.subscribe(params => (this.id = params['id']));
  }

  ngOnInit() {
    
    this.limuniModel.dajLimunPoId(this.id,(limun)=>{
      
      this.limun = limun;
    });
  }

  colors = colors;
}
