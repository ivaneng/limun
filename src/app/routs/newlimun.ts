import { Component } from "@angular/core";
import { LimuniModel } from "../services/limuni.model";
import { Router } from "@angular/router";
import { colors } from '../configs/colors';
@Component({
  templateUrl: "./newlimun.html"
})
export class NewLimun {
  constructor(private limunimodel: LimuniModel, private router: Router) {}

 

  limun = {};
  colors=colors;  
  dodajLimun() {
    if (this.limun["weight"] && this.limun["nutritionValue"]) {
      this.limunimodel.dodajLimun(this.limun, () => {
        this.router.navigate(["/limuni"]);
      });
    } else {
      alert("Nije sve definisano");
    }
  }
}
