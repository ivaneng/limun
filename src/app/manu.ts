import { Component } from '@angular/core';

@Component({
    selector:'manu-list',
    templateUrl:'./manu.html'
})

export class Manu {

    links= [
        {
            naziv:'Home',
            putanja:"/",
            jesamAktivan:false
        },
        {
            naziv:'Limuni',
            putanja:"/limuni",
            jesamAktivan:false
        },
        {
            naziv:'Dodaj Limun',
            putanja:"/limun/new",
            jesamAktivan:false
        }

    ]

    limun = {};
    dodajLimun() {
        console.log(this.limun);
    }


}
