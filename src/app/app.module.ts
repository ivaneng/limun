import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HomeView } from './routs/HomeView';
import { JedanLimun } from './routs/JedanLimun';
import { LimunView } from './routs/LimunView';

import {Manu} from './manu';
import {Edit} from './routs/jedanlimun/edit';
import {Info} from './routs/jedanlimun/info';
import { LimuniService } from './services/limuni.services';
import { LimuniModel} from './services/limuni.model';
import {JedanLimunDelete} from './routs/jedanlimun/delete';
import { NewLimun } from './routs/newlimun';
import { SearchPipe } from './pipe/search.pipe';
const routs:Routes = [
  {path:'',component:HomeView},
  {
    path:'limuni/:id',component:JedanLimun,
    children: [
    {path:'',redirectTo:'info', pathMatch: 'full'},
    {path:'edit',component:Edit},
    {path:'info',component:Info},
    {path:'delete',component:JedanLimunDelete}
  ]},
  {path:'limuni/:id',component:JedanLimun},
  {path:'limun/new',component:NewLimun},
  {path:'limuni',component:LimunView},
  {path:'**',redirectTo:'/'}
]
  
@NgModule({
  declarations: [
    AppComponent,
    HomeView,
    JedanLimun,
    LimunView,
    Manu,
    Edit,
    Info,
    JedanLimunDelete,
    NewLimun,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routs),
    HttpModule,
    FormsModule
  ],
  providers: [LimuniService,LimuniModel],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
