export const colors = [
  {
    label: "Red",
    key: "red"
  },
  {
    label: "White",
    key: "white"
  },
  {
    label: "Green",
    key: "green"
  },
  {
    label: "Black",
    key: "black"
  }
];
